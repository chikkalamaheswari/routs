import { AbstractControl } from '@angular/forms';

export interface Cvalidator extends AbstractControl { warnings: any; }

export function mobileWarning(controls: Cvalidator) {
  console.log("@@@@@@@@@@",controls.value);
  if (!controls.value) { 
    return null;
   }
  let val = +controls.value;
  controls.warnings = val > 10 ? { toMobileWarning: {val} } : null;
  return controls;
}

