import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { mobileWarning, Cvalidator} from '../cvalidator';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  public form : FormGroup
  constructor(private _fb: FormBuilder) { }
  ngOnInit() {
    this.form = this._fb.group({
      mobileNo: ['', [
        Validators.required,
        mobileWarning]]
    })
    
  }
  get mobileNo() {
    return this.form
 }

}
