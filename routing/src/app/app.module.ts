import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ServiceComponent } from './service/service.component';
import { ContactComponent } from './contact/contact.component';
import { AuthService } from './auth.service';
import { CanActivateRouteGuard } from './can-activate-route.guard';
import { JwtModule, JwtModuleOptions, JWT_OPTIONS, JwtHelperService } from "@auth0/angular-jwt";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ServiceComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('token');
        },
        whitelistedDomains: ['localhost:4200']
      }
    })
  ],
  providers: [ 
    AuthService, 
    CanActivateRouteGuard,
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
        JwtHelperService
   ],
  bootstrap: [AppComponent]
})
export class AppModule { }
