import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  form: FormGroup;
  ordersData = [
    { id: 100, name: 'order 1' },
    { id: 200, name: 'order 2' },
    { id: 300, name: 'order 3' },
    { id: 400, name: 'order 4' }
  ];

  constructor(private formBuilder: FormBuilder,private auth: AuthService) {
    this.form = this.formBuilder.group({
      orders: new FormArray([])
    });

    this.addCheckboxes();
  }
  
  ngOnInit(){
    setTimeout(()=>{
  this.auth.tokenFlag= false;
    },15000);
   
    }

     addCheckboxes() {
      this.ordersData.forEach((o, i) => {
        const control = new FormControl(i === 0); // if first item set to true, else false
        (this.form.controls.orders as FormArray).push(control);
      });
    }


    submit() {
      const selectedOrderIds = this.form.value.orders
      selectedOrderIds.forEach(data=>
        {
          console.log(data)
        })
      console.log(selectedOrderIds)
    }
   
}
