import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  form: FormGroup;


  ngOnInit() {
    this.form = new FormGroup({
      'searchBox': new FormControl(null, Validators.required)
    });
  }

  addText(value) {
    this.form.setValue({
      'searchBox': value
    });
    console.log(value)
  }
  saverange(value)
  {
    this.form =value;
    console.log(this.form)
  }

}
